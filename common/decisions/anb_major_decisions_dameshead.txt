﻿
# Consolidate Arbaran
consolidate_arbaran_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_realm.dds"
	major = yes
	ai_goal = yes
	desc = consolidate_arbaran_decision_desc
	
	cost = {
		gold = major_gold_value
		prestige = major_prestige_value
	}

	is_shown = {
		is_ruler = yes
		is_landed = yes
	
		highest_held_title_tier < tier_empire
		primary_title = title:k_arbaran
		
		NOT = {
			is_target_in_global_variable_list = {
				name = unavailable_unique_decisions
				target = flag:consolidate_arbaran_decision
			}
		}
	}

	is_valid = {
		completely_controls_region = custom_arbaran
		prestige_level >= 3
	}
	
	is_valid_showing_failures_only = {
		is_capable_adult = yes
		is_imprisoned = no
		is_independent_ruler = yes
		is_at_war = no
	}

	effect = {
		save_scope_as = arbaran_former

		show_as_tooltip = { consolidate_arbaran_decision_effect = yes } #Actually applied in anb_decision_major_events.0003 - prestige, laws, title changes

		#Events
		trigger_event = anb_dameshead_events.0001
		every_player = {
			limit = {
				NOT = { this = scope:arbaran_former }
				is_within_diplo_range = { CHARACTER = scope:arbaran_former }
			}
			trigger_event = anb_dameshead_events.0002
		}

		#Can only be done once
		add_to_global_variable_list = {
			name = unavailable_unique_decisions
			target = flag:consolidate_arbaran_decision
		}
		set_global_variable = {
			name = consolidate_arbaran_decision
			value = scope:arbaran_former
		}
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 1000
	}
}

# Create Arbarani Culture
encourage_arbarani_decision = {
	picture = "gfx/interface/illustrations/decisions/decision_realm.dds"
	major = yes
	ai_check_interval = 120
	desc = encourage_arbarani_decision_desc
	
	cost = {
		prestige = major_prestige_value
	}

	is_shown = {
		has_title = title:k_arbaran
		
		NOT = {
			is_target_in_global_variable_list = {
				name = unavailable_unique_decisions
				target = flag:encourage_arbarani_decision
			}
		}
		
		is_target_in_global_variable_list = {
			name = unavailable_unique_decisions
			target = flag:consolidate_arbaran_decision
		}
		
		OR = {
			culture = culture:damerian
			culture = culture:morbanite
			culture = culture:moon_elvish
			culture = {
				any_parent_culture = { 
					OR = {
						this = culture:damerian
						this = culture:morbanite
						this = culture:moon_elvish
					}
				}
			}
		}
	}

	is_valid = {
		completely_controls = title:k_arbaran
		
		trigger_if = {
			limit = { NOT = { culture = culture:morbanite } }
			culture = {
				cultural_acceptance = { 
					target = culture:morbanite
					value >= 40
				}
			}
		}
		trigger_if = {
			limit = { NOT = { culture = culture:crownsman } }
			culture = {
				cultural_acceptance = { 
					target = culture:crownsman
					value >= 40
				}
			}
		}
		
		prestige_level >= 3
	}

	effect = {
		save_scope_as = arbarani_creator

		show_as_tooltip = { create_arbarani_effect = yes } #Actually applied in anb_decision_major_events.0003 - prestige, laws, title changes

		#Events
		trigger_event = anb_dameshead_events.0003
		every_vassal_or_below = {
			limit = {
				NOT = { this = scope:arbarani_creator }
				OR = {
					culture = culture:morbanite
					capital_province = {
						geographical_region = custom_arbaran
					}
				}
			}
			trigger_event = anb_dameshead_events.0004
		}

		#Can only be done once
		add_to_global_variable_list = {
			name = unavailable_unique_decisions
			target = flag:encourage_arbarani_decision
		}
		set_global_variable = {
			name = encourage_arbarani_decision
			value = scope:arbarani_creator
		}
	}

	ai_potential = {
		always = yes
	}

	ai_will_do = {
		base = 1000
	}
}

