﻿mukarron = { #Air nomads
	color = { 97 166 255 }

	ethos = ethos_stoic
	heritage = heritage_fangaulan
	language = language_binwarji
	martial_custom = martial_custom_equal
	traditions = {
		tradition_land_of_authority
		tradition_caravaneers
		tradition_nubian_warrior_queens
		tradition_hit_and_run
		
	}
	
	name_list = name_list_fangaulan
	
	coa_gfx = { west_african_group_coa_gfx }
	building_gfx = { african_building_gfx }
	clothing_gfx = { african_clothing_gfx }
	unit_gfx = { sub_sahran_unit_gfx }
	
	ethnicities = {
		10 = african
	}
}

ndurubu = { #Lion Hearted
	color = { 255 255 0 }

	ethos = ethos_bellicose
	heritage = heritage_fangaulan
	language = language_binwarji
	martial_custom = martial_custom_equal
	traditions = {
		tradition_tribe_unity
		tradition_warrior_culture
		tradition_horse_breeder
		tradition_stand_and_fight
	}
	
	name_list = name_list_fangaulan
	
	coa_gfx = { west_african_group_coa_gfx }
	building_gfx = { african_building_gfx }
	clothing_gfx = { african_clothing_gfx }
	unit_gfx = { sub_sahran_unit_gfx }
	
	ethnicities = {
		100 = ndurubu
	}
}

taafi = { #Sarhaly nomads
	color = { 25 88 0 }

	ethos = ethos_egalitarian
	heritage = heritage_fangaulan
	language = language_binwarji
	martial_custom = martial_custom_equal
	traditions = {
		tradition_land_of_authority
		tradition_diasporic
		tradition_culinary_art
	}
	dlc_tradition = {
		trait = tradition_music_theory
		requires_dlc_flag = royal_court
	}
	
	name_list = name_list_fangaulan
	
	coa_gfx = { west_african_group_coa_gfx }
	building_gfx = { african_building_gfx }
	clothing_gfx = { african_clothing_gfx }
	unit_gfx = { sub_sahran_unit_gfx }
	
	ethnicities = {
		10 = african
	}
}