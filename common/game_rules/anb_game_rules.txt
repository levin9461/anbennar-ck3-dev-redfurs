﻿# Anbennar - disabled generate family by default and removed mongol and hungarian invasions

racial_legitimacy_and_supremacy = {
	default = legitimacy_and_supremacy_on

	categories = {
		game_modes
	}

	legitimacy_and_supremacy_on = {
	}
	
	legitimacy_and_supremacy_off = {
	}
}

marked_by_destiny = {
	default = enable_marked_by_destiny

	categories = {
		difficulty ai
	}

	enable_marked_by_destiny = {
	}
	
	disable_marked_by_destiny = {
	}
}

random_races = {
	default = no_random_races

	categories = {
		game_modes
	}

	no_random_races = {
	}

	starting_randomized_races = {
	}
	
	all_randomized_races = {
	}
}

racial_compatibility = {
	default = racial_compatibility_on

	categories = {
		game_modes
	}

	racial_compatibility_on = {
	}
	
	racial_compatibility_off = {
	}
}
