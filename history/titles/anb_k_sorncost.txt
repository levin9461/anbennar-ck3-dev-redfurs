k_sorncost = {
	1000.1.1 = { change_development_level = 8 }
	983.5.1 = {
		holder = 511	#Petran of Sorncost
	}
	1019.2.5 = {
		holder = 8	#Alcuin sil Sorncost
	}
}

d_sorncost = {
	1000.1.1 = { change_development_level = 8 }
}

c_sorncost = {
	1000.1.1 = { change_development_level = 12 }
	983.5.1 = {
		holder = 511	#Petran of Sorncost
	}
	1019.2.5 = {
		holder = 8	#Alcuin sil Sorncost
	}
}

c_fioncavin = {
	1000.1.1 = { change_development_level = 11 }
	979.7.13 = {
		holder = uinteios_0001
	}
	984.1.1 = {
		liege = k_sorncost
	}
}

d_sormanni_hills = {
	1000.1.1 = { change_development_level = 7 }
}

c_coruan = {
	1000.1.1 = { change_development_level = 9 }
}

d_venail = {
	1000.1.1 = { change_development_level = 5 }
	1021.10.31 = {
		succession_laws = { elven_elective_succession_law elf_only }
		holder = 40004
		liege = k_sorncost
	}
}

c_venail = {
	1000.1.1 = { change_development_level = 8 }
	1021.10.31 = {
		holder = 40004
		liege = k_sorncost
	}
}

c_firstsight = {
	1000.1.1 = { change_development_level = 5 }
	1021.10.31 = {
		holder = 40004
		liege = d_venail
	}
}

c_edilliande = {
	1000.1.1 = { change_development_level = 5 }
	1021.10.31 = {
		holder = 40005
		liege = d_venail
	}
}