#k_firanyalen
##d_firanyalen
###c_firanyalen
676 = {		#Firanyalen

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_ferone
677 = {		#Ferone

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_hystara
678 = {		#Hystara

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_uemorena
675 = {		#Uemorena

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

##d_uvolate
###c_uvolate
672 = {		#Uvolate

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_harppents
673 = {		#Harppents

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

###c_screemant
674 = {		#Screemant

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}

##d_hranapas
###c_hranapas
668 = {		#Hranapas

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_lovers_roost
669 = {		#Lovers' Roost

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_arkasul
671 = {		#Arkasul

    # Misc
    culture = gelkar
    religion = old_bulwari_sun_cult
	holding = tribal_holding

    # History
}

###c_shrillek
670 = {		#Shrillek

    # Misc
    culture = damerian
    religion = cult_of_the_dame
	holding = tribal_holding

    # History
}